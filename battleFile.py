from pygame import event, Surface, display, draw, mouse, key, QUIT, KEYDOWN, K_a, K_d, K_w, K_s, K_LCTRL, K_ESCAPE, MOUSEBUTTONDOWN
from valuesFile import *
from msgFile import *
from objFile import *

#Small status windows for selected unit in lower-left (showing moveDist, attackDist, jump, hp/gauges as numbers)
#Process actions (for attack, item, skill)
#Item/skill screens
#Details screen
##Enemy AI
#Pause menu
#During enemy turn, screen centres on active enemy

#Ninja attacking image
#Fixed bug with attacking
#Units switch to attacking animation when attacking
#Health bars are now drawn above units' heads
#Jumping is now a smooth motion - jumping has 2 frames setting up, then 10 frames jumping
#Animations added - one can play at a time (is this sufficient?) - note health bars don't draw during animations
#Attacking draws an attack animation and then animated health loss
#Attack animations - [[x0, y0], [x1], [x0, y0, z0]]; all x play concurrently, when x completed it is removed and y becomes x (etc)

class Battle():
    def __init__(self, graphics, terrain, mData, loc, objects, party, enemies):
        self.g = graphics
        self.terrain = terrain
        self.mData = mData
        self.objects = objects
        self.party = party
        self.enemies = enemies
        self.mapImg = Surface((SCREENW, SCREENH)).convert_alpha()
        self.objImg = Surface((SCREENW, SCREENH)).convert_alpha()

        self.maxX = max([x for x, y in self.mData.keys()])
        self.maxY = max([y for x, y in self.mData.keys()])

        self.scrollOffset = list(self.getPixelLoc(loc))
        self.unitMoveOffset = [0, 0]
        self.mouseLoc = [0, 0]
        self.turn = 0
        self.jumpPauseStep = 0
        self.moveList = []
        self.clickAction = None
        self.animationPaused = False
        self.activeObj = None
        self.animations = []
        self.activeRoute = []
        self.moveRoutes = {}
        self.attackTiles = []
        self.routesImg = Surface((SCREENW, SCREENH)).convert_alpha()
        self.statusImg = Surface((0, 0))

        self.drawMap()
        self.drawHUDRoutes()

    def drawMap(self):
        width = (TILEW / 2) * (max([y - x for x, y in self.mData.keys()]) + max([x - y for x, y in self.mData.keys()]) + 2)
        height = MAPYOFFSET + (TILED / 2) * (max([x + y for x, y in self.mData.keys()]) + 2) + TILEH * (self.mData[(0, 0)][1] + 2)
        self.mapImgBase = Surface((width, height)).convert_alpha()
        self.mapImgBase.fill((0, 0, 0, 0))
        for (x, y) in sorted(self.mData.keys()):
            left, top = self.getPixelLoc((x, y))
            top -= TILEH * self.mData[(x, y)][1]
            #Draw tile
            self.mapImgBase.blit(self.g["m_t_" + self.mData[(x, y)][0]], (left, top))
            #Draw top-left edge
            if (x - 1, y) not in self.mData.keys() or self.mData[(x, y)][1] > self.mData[(x - 1, y)][1]:
                self.mapImgBase.blit(self.g["m_tel_" + self.mData[(x, y)][0]], (left, top))
            #Draw top-right edge
            if (x, y - 1) not in self.mData.keys() or self.mData[(x, y)][1] > self.mData[(x, y - 1)][1]:
                self.mapImgBase.blit(self.g["m_ter_" + self.mData[(x, y)][0]], (left + (TILEW / 2), top))
            #Draw fill
            leftFillOffset, rightFillOffset = self.mData[(x, y)][1] + 1, self.mData[(x, y)][1] + 1
            if (x, y + 1) in self.mData.keys(): leftFillOffset -= self.mData[(x, y + 1)][1] + 2
            if (x + 1, y) in self.mData.keys(): rightFillOffset -= self.mData[(x + 1, y)][1] + 2
            for layer in range(0, leftFillOffset):
                self.mapImgBase.blit(self.g["m_bl_" + self.terrain], (left, top + (TILED / 2) + TILEH * (layer + 1)))
            for layer in range(0, rightFillOffset):
                self.mapImgBase.blit(self.g["m_br_" + self.terrain], (left + (TILEW / 2), top + (TILED / 2) + TILEH * (layer + 1)))
            self.mapImgSize = self.mapImgBase.get_size()
        self.subsurfaceMap()

    def drawHUDRoutes(self):
        self.drawCharHUD()
        self.drawActionHUD()
        self.drawStatusHUD()
        self.genRoutes()

    def drawCharHUD(self):
        self.charHUDImg = Surface((CHARHUDW, CHARHUDH)).convert_alpha()
        self.charHUDImg.fill((0, 0, 0, 0))
        for i, char in enumerate(self.party):
            healthPercent = char.hp/char.maxHP
            self.charHUDImg.blit(self.g["HUDHealthBase"], (0, CHARHUDICONH * i))
            health = Surface((51, 51))
            health.fill((0, 0, 1))
            health.blit(self.g["HUDHealthBar"], (0, 0))
            for point in HEALTHPOINTS[:max(int(138 * (1 - healthPercent))-2, 0)]:
                draw.line(health, (0,0,1), (25, 25), point, 2)
            health.set_colorkey((0,0,1))
            self.charHUDImg.blit(health, (0, CHARHUDICONH * i))
            for j, gauge in enumerate(char.gauges):
                self.charHUDImg.blit(self.g["HUDGaugeBase"], (42 + 12 * j, 33 + CHARHUDICONH * i))
                gaugeOffset = int(18 * (gauge.cap/gauge.maxCap))
                self.charHUDImg.blit(self.g["HUDGauge"+gauge.element.capitalize()].subsurface(0, 18 - gaugeOffset, 11, gaugeOffset), (42 + 12 * j, 51 + CHARHUDICONH * i - gaugeOffset))
                self.charHUDImg.blit(self.g["HUDGaugeSymbol"+gauge.element.capitalize()], (42 + 12 * j, 33 + CHARHUDICONH * i))
            if healthPercent > 0:
                self.charHUDImg.blit(self.g["HUDBase"+char.status], (7, 7 + CHARHUDICONH * i))
            else:
                self.charHUDImg.blit(self.g["HUDBaseDeath"], (7, 7 + CHARHUDICONH * i))
            self.charHUDImg.blit(self.g["HUDFace" + char.Class.capitalize()], (7, 7 + CHARHUDICONH * i))
            self.charHUDImg.blit(self.g["HUDLevelBase"], (44, 1 + CHARHUDICONH * i))
            levelText = genText("%02.f" % char.level, 18)
            levelTextSize = levelText.get_size()
            self.charHUDImg.blit(levelText, (55 - levelTextSize[0] / 2, 12 + CHARHUDICONH * i - levelTextSize[1] / 2))
            icons = []
            if not char.moved: icons += ["Moved"]
            if not char.attacked: icons += ["Attacked"]
            for j in range((not char.moved) + (not char.attacked)):
                self.charHUDImg.blit(self.g["HUDIcon" + icons[j]], ((62, 16 + CHARHUDICONH * i), (74, 5 + CHARHUDICONH * i))[j])
        self.charHUDImg.blit(self.g["HUDButtonUndo"+"Grey" * (len(self.moveList) == 0)], (5, CHARHUDICONH * MAXPARTY + 1))
        self.charHUDImg.blit(self.g["HUDButtonCentre"+"Grey" * (self.activeObj == None)], (50, CHARHUDICONH * MAXPARTY + 1))
        self.charHUDImg.convert_alpha()

    def drawActionHUD(self):
        self.actionHUDImg = Surface((ACTIONHUDW, ACTIONHUDH)).convert_alpha()
        self.actionHUDImg.fill((0, 0, 0, 0))
        if self.activeObj in self.party and not self.activeObj.attacked:
            self.actionHUDImg.blit(self.g["HUDButtonAction"], (0, 0))
            self.actionHUDImg.blit(self.g["skillText"], (HUDBUTTONW / 2 - self.g["skillText"].get_size()[0]/2, 10))
            self.actionHUDImg.blit(self.g["HUDButtonAction"], (HUDBUTTONW + 5, 0))
            self.actionHUDImg.blit(self.g["itemText"], (1.5 * HUDBUTTONW + 5 - self.g["itemText"].get_size()[0]/2, 10))
        else:
            self.actionHUDImg.blit(self.g["HUDButtonActionHidden"], (0, 0))
            self.actionHUDImg.blit(self.g["HUDButtonActionHidden"], (HUDBUTTONW + 5, 0))
        self.actionHUDImg.blit(self.g["HUDButtonAction"], (ACTIONHUDW - 2 * HUDBUTTONW - 5, 0))
        self.actionHUDImg.blit(self.g["detailsText"], (ACTIONHUDW - 1.5 * HUDBUTTONW - 5 - self.g["detailsText"].get_size()[0]/2, 10))
        self.actionHUDImg.blit(self.g["HUDButtonAction"], (ACTIONHUDW - HUDBUTTONW, 0))
        self.actionHUDImg.blit(self.g["endTurnText"], (ACTIONHUDW - HUDBUTTONW/2 - self.g["endTurnText"].get_size()[0]/2, 10))

    def drawObjects(self):
        self.objImg.fill((0, 0, 0, 0))
        for obj in self.objects + self.party + self.enemies:
            drawLoc = self.getDrawLoc(obj.loc)
            if obj == self.activeObj:
                drawLoc = [int(drawLoc[0] + self.unitMoveOffset[0]), int(drawLoc[1] + self.unitMoveOffset[1])]
            if tuple(obj.loc) in self.mData.keys(): drawLoc[1] -= TILEH * self.mData[tuple(obj.loc)][1]
            self.objImg.blit(obj.img, (drawLoc[0] + (TILEW / 2) - obj.imgSize[0]/2, drawLoc[1] + UNITYOFFSET - obj.imgSize[1]/2))
            if self.animations == []:
                self.objImg.blit(self.g["mapHealthBase"], (drawLoc[0] + (TILEW / 2) - 20, drawLoc[1] + UNITYOFFSET - 32))
                self.objImg.blit(self.g["mapHealthBar"].subsurface(0, 0, 40 * (obj.hp/obj.maxHP), 8), (drawLoc[0] + (TILEW / 2) - 20, drawLoc[1] + UNITYOFFSET - 32))

    def drawStatusHUD(self):
        if self.activeObj != None:
            self.statusImg = self.g["statusBackImg"].copy()
            for i, t in enumerate(("HIT", "ATK", "DEF", "MOV", "JMP")):
                self.statusImg.blit(genText(t, 20, colour = (0, 0, 0)), (10, 10 + 25 * i))

    def drawRoutes(self):
        #Draw image for self.routes
        self.routesImg.fill((0, 0, 0, 0))
        for tile in self.moveRoutes.keys():
            drawLoc = self.getDrawLoc(tile)
            offsets = self.getTileCoverOffsets(tile)
            self.routesImg.blit(self.g["tileMove_" + offsets[0]], (drawLoc[0], drawLoc[1] - TILEH * self.mData[tuple(tile)][1]))
            self.routesImg.blit(self.g["tileMove_" + offsets[1]], (drawLoc[0] + 64, drawLoc[1] - TILEH * self.mData[tuple(tile)][1]))
        for tile in self.attackTiles:
            drawLoc = self.getDrawLoc(tile)
            offsets = self.getTileCoverOffsets(tile)
            self.routesImg.blit(self.g["tileAttack_" + offsets[0]], (drawLoc[0], drawLoc[1] - TILEH * self.mData[tuple(tile)][1]))
            self.routesImg.blit(self.g["tileAttack_" + offsets[1]], (drawLoc[0] + 64, drawLoc[1] - TILEH * self.mData[tuple(tile)][1]))

    def drawDamageAnimation(self, maxHP, startHP, endHP):
        imgs = []
        diff = 40 * (startHP/maxHP-endHP/maxHP) / 6
        for x in range(6):
            imgs += [self.g["mapHealthBase"].copy()]
            imgs[-1].blit(self.g["mapHealthBar"].subsurface(0, 0, 40 * (startHP/maxHP) - diff * x, 8), (0, 0))
        return imgs

    def subsurfaceMap(self):
        self.mapImg.fill((0, 0, 0, 0))
        self.mapImg.blit(self.mapImgBase, (SCREENW/2 - self.scrollOffset[0], SCREENH/2 - self.scrollOffset[1]))
        self.drawObjects()
        self.drawRoutes()

    def centreScreen(self, loc):
        self.scrollOffset = list(self.getPixelLoc(loc))
        self.subsurfaceMap()

    def tickObjects(self):
        for obj in self.party + self.enemies + self.objects:
            obj.tickImg()
        self.drawObjects()

    def tickAnimation(self):
        for i, animationSet in enumerate(self.animations):
            if animationSet[0].tickImg():
                self.animations[i].pop(0)
        while [] in self.animations:
            self.animations.remove([])

    def updateMove(self):
        if self.activeRoute != []:
            self.activeObj.changeDirection(self.activeRoute[0][1])
            heightDifference = self.mData[tuple(self.activeRoute[0][0])][1] - self.mData[tuple(self.activeObj.loc)][1]
            if self.activeObj.action == "walking" and heightDifference != 0:
                self.activeObj.changeAction("jumping")
                self.jumpPauseStep = 2
            elif self.jumpPauseStep > 0:
                self.jumpPauseStep -= 1
            else:
                for x, mult in enumerate(((1, 1), (-1, 1), (-1, -1), (1, -1))):
                    if self.activeRoute[0][1] == x:
                        self.unitMoveOffset = [self.unitMoveOffset[0] + mult[0] * CHARSPEED, self.unitMoveOffset[1] + mult[1] * CHARSPEED/2]
                        if self.activeObj.action == "jumping":
                            if heightDifference > 0:
                                if abs(self.unitMoveOffset[0]) <= 51.2:
                                    self.unitMoveOffset[1] -= heightDifference + 2
                                elif abs(self.unitMoveOffset[0]) < 64:
                                    self.unitMoveOffset[1] += 8
                            else:
                                if abs(self.unitMoveOffset[0]) <= 12.8:
                                    self.unitMoveOffset[1] -= 8
                                elif abs(self.unitMoveOffset[0]) < 64:
                                    self.unitMoveOffset[1] -= heightDifference - 2
            if abs(self.unitMoveOffset[0]) >= 64:
                if self.activeObj.action == "jumping": self.activeObj.changeAction("walking")
                self.activeObj.loc = self.activeRoute[0][0]
                del self.activeRoute[0]
                self.unitMoveOffset = [0, 0]
            if len(self.activeRoute) == 0:
                self.animationPaused = False
                self.activeObj.changeAction("idle")
                self.drawHUDRoutes()
        elif self.activeObj.action == "attacking":
            if self.activeObj.imgNo == len(self.activeObj.imgSet["attacking"][self.activeObj.direction]) - 1:
                self.animationPaused = False
                self.activeObj.changeAction("idle")
                self.drawHUDRoutes()

    def genRoutes(self):
        self.attackTiles = []
        self.moveRoutes = {}
        if self.activeObj in self.party:
            enemyLocs = [enemy.loc for enemy in self.enemies]
            objectLocs = [obj.loc for obj in self.objects]
            self.moveRoutes[tuple(self.activeObj.loc)] = []
            current = [self.activeObj.loc]
            nextItr = []
            for distance in range(1, self.activeObj.moveDist * int(not self.activeObj.moved) + 1):
                for tile in current:
                    sweepA = ([tile[0]+1,tile[1]], [tile[0],tile[1]+1], [tile[0]-1,tile[1]], [tile[0],tile[1]-1])
                    sweepB = ([tile[0],tile[1]-1], [tile[0]-1,tile[1]], [tile[0],tile[1]+1], [tile[0]+1,tile[1]])
                    for direction, adjacentTile in enumerate((sweepA, sweepB)[(distance%2) ^ 1]):
                        if tuple(adjacentTile) in self.mData.keys() and tuple(adjacentTile) not in self.moveRoutes.keys():
                            if self.mData[tuple(adjacentTile)][2] and not (tuple(adjacentTile) in objectLocs and not self.objects[tuple(adjacentTile)].passable) and adjacentTile not in enemyLocs:
                                if self.mData[tuple(adjacentTile)][1] <= self.mData[tuple(tile)][1] + self.activeObj.jump:
                                    nextItr.append(adjacentTile)
                                    if (distance%2) ^ 1: direction ^= 3
                                    self.moveRoutes[tuple(adjacentTile)] = self.moveRoutes[tuple(tile)] + [[adjacentTile, direction]]
                current = [x for x in nextItr]
                nextItr = []
            for char in self.party:
                if tuple(char.loc) in self.moveRoutes.keys():
                    self.moveRoutes.pop(tuple(char.loc))
            #Calculate attack routes for active unit
            if not self.activeObj.attacked:
                for enemyLoc in enemyLocs:
                    if abs(enemyLoc[0] - self.activeObj.loc[0]) + abs(enemyLoc[1] - self.activeObj.loc[1]) <= self.activeObj.attackDist:
                        if (self.mData[tuple(enemyLoc)][1] - self.mData[tuple(self.activeObj.loc)][1]) <= 2 * self.activeObj.attackDist:
                            if self.getLOS(self.activeObj.loc, enemyLoc, self.activeObj.attackDist):
                                self.attackTiles.append(enemyLoc)
        self.drawRoutes()

    def getPixelLoc(self, loc):
        #Get cartesian drawing location from isometric loc relative to top-left of map image
        return ((TILEW / 2) * (loc[0] - loc[1] + max([y - x for x, y in self.mData.keys()])), (TILED / 2) * (loc[0] + loc[1] + 1) + TILEH * self.mData[(0, 0)][1])

    def getDrawLoc(self, loc):
        #Get cartesian drawing location from isometric loc relative to top-left of screen
        drawLoc = self.getPixelLoc(loc)
        return [SCREENW/2 + drawLoc[0] - self.scrollOffset[0], SCREENH/2 + drawLoc[1] - self.scrollOffset[1]]

    def getMouseLoc(self):
        #Get isometric map tile from cartesian mouse cursor location
        mouseCoord = list(mouse.get_pos())
        if CHARHUDX < mouseCoord[0] < CHARHUDX + CHARHUDW and CHARHUDY < mouseCoord[1] < CHARHUDY + CHARHUDH:
            if self.charHUDImg.get_at((mouseCoord[0] - CHARHUDX, mouseCoord[1] - CHARHUDY)) != (0, 0, 0, 0):
                return
        if ACTIONHUDX < mouseCoord[0] < ACTIONHUDX + ACTIONHUDW and ACTIONHUDY < mouseCoord[1] < ACTIONHUDY + ACTIONHUDH:
            if self.actionHUDImg.get_at((mouseCoord[0] - ACTIONHUDX, mouseCoord[1] - ACTIONHUDY)) != (0, 0, 0, 0):
                return
        mouseCoord[0] -= SCREENW/2 - self.scrollOffset[0] + (TILEW / 2) * max([y - x for x, y in self.mData.keys()]) + (TILEW / 2)
        mouseCoord[1] -= SCREENH/2 - self.scrollOffset[1] + (TILED / 2) + TILEH * self.mData[(0, 0)][1]
        for x, y in reversed(sorted(self.mData.keys())):
            height = TILEH * self.mData[(x, y)][1]
            if x == (mouseCoord[0] + 2 * (mouseCoord[1] + height))//TILEW and y == -(mouseCoord[0] - 2 * (mouseCoord[1] + height))//TILEW:
                return [x, y]

    def getLOS(self, loc1, loc2, attackDist):
        #Find if there is line-of-sight between two units
        dx, dy = abs(loc2[0] - loc1[0]), abs(loc2[1] - loc1[1])
        x, y = loc1
        offset = dx - dy
        if dx == dy: #Perfect diagonal always has LOS
            return True
        for i in range(dx + dy, 0, -1):
            if not self.mData[(x//1, y//1)][2] or [x//1, y//1] in [o.loc for o in self.enemies + self.objects] or (
                self.mData[(x//1, y//1)][1] - self.mData[tuple(loc1)][1]) > attackDist * 2:
                return False
            if offset > 0:
                x += -1 + 2 * (loc2[0] > loc1[0])
                offset -= dy * 2
            else:
                y += -1 + 2 * (loc2[1] > loc1[1])
                offset += dx * 2
        return True

    def getTileCoverOffsets(self, tile):
        tile = tuple(tile)
        leftOffset = 0
        rightOffset = 0
        bottomOffset = 0
        if (tile[0], tile[1] + 1) in self.mData.keys():
            leftOffset = min(max(0, self.mData[(tile[0], tile[1] + 1)][1] - self.mData[tile][1]), 8)
        if (tile[0] + 1, tile[1]) in self.mData.keys():
            rightOffset = min(max(0, self.mData[(tile[0] + 1, tile[1])][1] - self.mData[tile][1]), 8)
        if (tile[0] + 1, tile[1] + 1) in self.mData.keys():
            bottomOffset = min(max(0, self.mData[(tile[0] + 1, tile[1] + 1)][1] - self.mData[tile][1]), 8)
        return ("l{0}b{1}".format(leftOffset, bottomOffset), "r{0}b{1}".format(rightOffset, bottomOffset))

    def select(self):
        mouseCoord = mouse.get_pos()
        keys = key.get_pressed()
        if CHARHUDX < mouseCoord[0] < CHARHUDX + CHARHUDW:
            if CHARHUDY + MAXPARTY * CHARHUDICONH <= mouseCoord[1] <= CHARHUDH:
                if self.charHUDImg.get_at((mouseCoord[0] - CHARHUDX, mouseCoord[1] - CHARHUDY)) != (0, 0, 0, 0):
                    if (mouseCoord[0] - CHARHUDX)//(CHARHUDW / 2) == 0:
                        if len(self.moveList) > 0:
                            self.undoMove()
                            return
                    else:
                        if self.activeObj != None:
                            self.centreScreen(self.activeObj.loc)
                            return
        if not self.animationPaused:
            #Click button on Action HUD
            if ACTIONHUDX < mouseCoord[0] < ACTIONHUDX + ACTIONHUDW:
                if ACTIONHUDY < mouseCoord[1] < ACTIONHUDY + ACTIONHUDH:
                    gap = (ACTIONHUDW - 4 * HUDBUTTONW - 20) * (mouseCoord[0] > (HUDBUTTONW + 5) * 2 + ACTIONHUDX)
                    if self.actionHUDImg.get_at((mouseCoord[0] - ACTIONHUDX, mouseCoord[1] - ACTIONHUDY)) != (0, 0, 0, 0):
                        index = (mouseCoord[0] - gap - ACTIONHUDX)//(HUDBUTTONW + 5)
                        if self.activeObj != None and not self.activeObj.attacked:
                            (self.useSkill, self.useItem, self.openDetails, self.endTurn)[index]()
                        elif index >= 3:
                            (self.openDetails, self.endTurn)[index - 2]()
            #Select unit via HUD
            if CHARHUDX < mouseCoord[0] < CHARHUDX + CHARHUDW:
                if CHARHUDY < mouseCoord[1] < CHARHUDY + MAXPARTY * CHARHUDICONH:
                    if self.charHUDImg.get_at((mouseCoord[0] - CHARHUDX, mouseCoord[1] - CHARHUDY)) != (0, 0, 0, 0):
                        self.activeObj = self.party[(mouseCoord[1] - CHARHUDY)//CHARHUDICONH]
                        self.drawHUDRoutes()
                        if keys[K_LCTRL]:
                            self.centreScreen(self.activeObj.loc)
            #Select unit via map
            if self.activeObj != None and self.mouseLoc != None:
                #Player clicks to move
                if self.moveRoutes != {} and tuple(self.mouseLoc) in self.moveRoutes.keys():
                    self.moveList.append(self.activeObj)
                    self.moveUnit(self.mouseLoc)
                    self.drawHUDRoutes()
                    return
                #Player clicks to attack an enemy
                elif self.attackTiles != [] and self.mouseLoc in self.attackTiles:
                    self.attackUnit([enemy for enemy in self.enemies if enemy.loc == self.mouseLoc][0])
                    self.moveList = []
                    self.drawHUDRoutes()
                    return
            for char in self.party + self.enemies + self.objects:
                if self.mouseLoc == char.loc:
                    self.activeObj = char
                    self.drawHUDRoutes()
                    if keys[K_LCTRL]:
                        self.centreScreen(self.activeObj.loc)

    def deselect(self):
        if not self.animationPaused:
            self.activeObj = None
            self.moveRoutes = {}
            self.attackTiles = []
            self.drawHUDRoutes()

    def scroll(self, key):
        if key == K_a and self.scrollOffset[0] > 0: self.scrollOffset[0] -= SCROLLDIST
        elif key == K_w and self.scrollOffset[1] > 0: self.scrollOffset[1] -= SCROLLDIST
        elif key == K_d and self.scrollOffset[0] < self.mapImgSize[0]: self.scrollOffset[0] += SCROLLDIST
        elif key == K_s and self.scrollOffset[1] < self.mapImgSize[1]: self.scrollOffset[1] += SCROLLDIST
        self.subsurfaceMap()

    def undoMove(self):
        if self.animationPaused:
            self.animationPaused = False
            self.activeObj.changeAction("idle")
            self.unitMoveOffset = [0, 0]
        if len(self.moveList) > 0:
            self.activeObj = self.moveList[-1]
            self.activeObj.loc = self.activeObj.locStore
            self.activeObj.moved = False
            self.moveList = self.moveList[:-1]
            self.centreScreen(self.activeObj.loc)
            self.drawHUDRoutes()

    def moveUnit(self, target):
        self.animationPaused = True
        self.activeObj.moved = True
        self.activeRoute = self.moveRoutes[tuple(target)]
        self.activeObj.changeAction("walking")

    def attackUnit(self, target):
        self.animationPaused = True
        self.activeObj.attacked = True
        self.activeObj.changeAction("attacking")
        d = 0
        if self.activeObj.loc[1] < target.loc[1]: d = 1
        elif self.activeObj.loc[0] > target.loc[0]: d = 2
        elif self.activeObj.loc[1] > target.loc[1]: d = 3
        self.activeObj.changeDirection(d)
        damage = self.genDamage(self.activeObj, target)
        drawLoc = self.getDrawLoc(target.loc)
        self.animations += [[Animation("attack", [drawLoc[0] + 64, drawLoc[1] + 32], self.g["attackAnimations"]["01"])]] ############
        drawLoc = self.getDrawLoc(target.loc)
        if tuple(self.activeObj.loc) in self.mData.keys(): drawLoc[1] -= TILEH * self.mData[tuple(self.activeObj.loc)][1]
        self.animations[0] += [Animation("healthbar", (drawLoc[0] + (TILEW / 2), drawLoc[1] + UNITYOFFSET - 28), self.drawDamageAnimation(target.maxHP, target.hp,target.hp - damage), imgSpeed = 4)]
        target.hp -= damage

    def genDamage(self, unit, target):
        return 5

    def useItem(self):
        pass

    def useSkill(self):
        pass

    def openDetails(self):
        pass

    def endTurn(self):
        for unit in self.party:
            unit.locStore = unit.loc
            unit.moved = False
            unit.attacked = False
        self.activeObj = None
        self.moveList = []
        self.turn = 1
        self.drawHUDRoutes()

    def battleLoop(self, screen, clock):
        while True:
            clock.tick(BATTLEFPS)
            self.mouseLoc = self.getMouseLoc()
            keydown = key.get_pressed()
            for e in event.get():
                if e.type == QUIT:
                    display.quit()
                elif self.turn == 0:
                    if e.type == KEYDOWN and e.key == K_ESCAPE:
                        self.undoMove()
                    else:
                        if e.type == MOUSEBUTTONDOWN:
                            if mouse.get_pressed()[0]:
                                self.clickAction = self.select()
                            elif mouse.get_pressed()[2]:
                                self.deselect()
            self.tickObjects()
            self.tickAnimation()
            #Scrolling
            if self.animationPaused:
                self.updateMove()
            else:
                for k in (K_a, K_d, K_w, K_s):
                    if keydown[k]: self.scroll(k)
            #Turn shit
            if self.turn == 0: #Player
                screen.fill((150, 150, 200))
            elif self.turn == 1: #Enemy
                screen.fill((200, 120, 120))
            #Drawing
            screen.blit(self.g["backImg"], (0, 0))
            screen.blit(self.mapImg, (0, 0))
            #Routes image
            if (self.moveRoutes != {} or self.attackTiles != []) and not self.animationPaused:
                screen.blit(self.routesImg, (0, 0))
            #Selection for mouse
            if self.mouseLoc != None and self.turn == 0:
                selectDrawLoc = self.getDrawLoc(self.mouseLoc)
                offsets = self.getTileCoverOffsets(self.mouseLoc)
                screen.blit(self.g["selectionMouseLoc_" + offsets[0]], (selectDrawLoc[0], selectDrawLoc[1] - TILEH * self.mData[tuple(self.mouseLoc)][1]))
                screen.blit(self.g["selectionMouseLoc_" + offsets[1]], (selectDrawLoc[0] + 64, selectDrawLoc[1] - TILEH * self.mData[tuple(self.mouseLoc)][1]))
            #Selection for active unit
            if self.activeObj != None and not self.animationPaused:
                unitLoc = self.getDrawLoc(self.activeObj.loc)
                offsets = self.getTileCoverOffsets(self.activeObj.loc)
                screen.blit(self.g["selectionActiveUnit_" + offsets[0]], (unitLoc[0], unitLoc[1] - TILEH * self.mData[tuple(self.activeObj.loc)][1]))
                screen.blit(self.g["selectionActiveUnit_" + offsets[1]], (unitLoc[0] + 64, unitLoc[1] - TILEH * self.mData[tuple(self.activeObj.loc)][1]))
            #
            screen.blit(self.objImg, (0, 0))
            #Animations
            for animationSet in self.animations:
                screen.blit(animationSet[0].img, animationSet[0].loc)
            for enemy in self.enemies:
                enemyLoc = self.getDrawLoc(enemy.loc)
                screen.blit(self.g["indicatorEnemy"], (enemyLoc[0] + TILEW/2 - 7, enemyLoc[1] - 6 - enemy.imgSize[1]/2 - TILEH * self.mData[tuple(enemy.loc)][1]))
            for obj in self.objects:
                objLoc = self.getDrawLoc(obj.loc)
                screen.blit(self.g["indicatorObject"], (objLoc[0] + TILEW/2 - 7, objLoc[1] - 6 - obj.imgSize[1]/2 - TILEH * self.mData[tuple(obj.loc)][1]))
            screen.blit(self.charHUDImg, (CHARHUDX, CHARHUDY))
            if self.activeObj in self.party:
                screen.blit(self.g["HUDActiveUnit"], (CHARHUDX - 5, CHARHUDY - 6 + CHARHUDICONH * self.party.index(self.activeObj)))
            screen.blit(self.actionHUDImg, (ACTIONHUDX, ACTIONHUDY))
            if self.activeObj != None:
                screen.blit(self.statusImg, (STATUSHUDX, STATUSHUDY))
            display.flip()
