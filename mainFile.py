from pygame import display, time, image
from os import listdir
from battleFile import *
from valuesFile import *
from objFile import *

class Game():
    def __init__(self):
        self.screen = display.set_mode((SCREENW, SCREENH))
        self.clock = time.Clock()
        self.loadG()
        self.mode = "battle"
        char = [Character(self.g["character"]["ninja"], [6 + x%2, 4 + x//2], "ninja", 1, 20, 20, 5, 2) for x in range(1)]
        enemy = Enemy(self.g["character"]["ninja"], [7, 4], "enemy", 1, 20, 4, 3, 1)
        enemy2 = Enemy(self.g["character"]["ninja"], [4, 5], "enemy", 1, 20, 4, 3, 1)
        enemy3 = Enemy(self.g["character"]["ninja"], [5, 7], "enemy", 1, 20, 4, 3, 1)
        enemy4 = Enemy(self.g["character"]["ninja"], [4, 6], "enemy", 1, 20, 4, 3, 1)
        for x in range(4): char[0].gauges.append(Gauge("decay", 10))
        self.battle = Battle(self.g["battle"], "dirt", m10x10, [5, 5],
            [], char, [enemy])
        self.mainLoop()

    def loadG(self):
        self.g = {"battle":{"attackAnimations":{}}, "character": {}}
        for filename in listdir("./graphics/battle"):
            if "." in filename: self.g["battle"][filename[:-4]] = image.load("./graphics/battle/" + filename).convert_alpha()
        for button in (("Skill", "skill"), ("Item", "item"), ("Details", "details"), ("End Turn", "endTurn")):
            t = genText(button[0], 40)
            self.g["battle"][button[1] + "Text"] = t
        for x in ("tileMove", "tileAttack", "selectionMouseLoc", "selectionActiveUnit"):
            for i, side in enumerate(("l", "r")):
                tile = image.load("./graphics/battle/" + x + ".png")
                tileCover = image.load("./graphics/battle/tileCover.png")
                for edge in range(9):
                    for bottom in range(9):
                        tile2 = tile.subsurface(i * 64, 0, 64, 63).copy()
                        tile2.blit(tileCover, (-64 + 64 * i, 32 - 8 * edge))
                        tile2.blit(tileCover, (-64 * i, 64 - 8 * bottom))
                        tile2.set_colorkey((0, 0, 0))
                        self.g["battle"][x + "_" + side + str(edge) + "b" + str(bottom)] = tile2.convert_alpha()
        for x in listdir("./graphics/battle/attackAnimations/"):
            img = image.load("./graphics/battle/attackAnimations/"+x).convert_alpha()
            self.g["battle"]["attackAnimations"][x[:-4]] = [img.subsurface(x * 100, 0, 100, 100) for x in range(img.get_size()[0]//100)]
        for char in listdir("./graphics/characters"):
            self.g["character"][char] = {}
            for action in set([x.split("-")[0] for x in listdir("./graphics/characters/" + char)]):
                self.g["character"][char][action] = {}
                for direction in range(4):
                    self.g["character"][char][action][direction] = {}
                    for frame in range(len([x for x in listdir("./graphics/characters/" + char) if (action + "-" + str(direction) in x)])):
                        self.g["character"][char][action][direction][frame] = image.load("./graphics/characters/" + char + "/" + action + "-" + str(direction) + str(frame) + ".png").convert_alpha()

    def mainLoop(self):
        while True:
            if self.mode == "town":
                pass
            elif self.mode == "menu":
                pass
            elif self.mode == "battle":
                self.mode = self.battle.battleLoop(self.screen, self.clock)

Game()
