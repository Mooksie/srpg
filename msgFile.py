from pygame import font
font.init()

def genText(text, size, colour = (0, 0, 0), cFont="Gabriola"):
    loadedFont = font.Font("./fonts/" + cFont + ".ttf", size)
    return loadedFont.render(text, True, colour)
