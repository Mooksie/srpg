class Unit():
    def __init__(self, imgSet, loc, Class, level, hp, jump, moveDist, attackDist):
        self.imgSet = imgSet
        self.loc = loc
        self.Class = Class
        self.level = level
        self.maxHP = hp
        self.hp = hp
        self.jump = jump
        self.moveDist = moveDist
        self.attackDist = attackDist

        self.status = "None"
        self.action = "idle"
        self.moved = False
        self.attacked = False
        self.direction = 0 #0 = down-right, clockwise
        self.imgNo = 0
        self.imgStep = 0
        self.img = self.imgSet[self.action][self.direction][self.imgNo]
        self.imgSize = self.img.get_size()

    def tickImg(self):
        self.imgStep += 1
        if (self.action == "idle" and self.imgStep == 3) or (self.action == "walking" and self.imgStep == 2) or (self.action == "attacking" and self.imgStep == 4):
            self.imgStep = 0
            self.imgNo = (self.imgNo + 1) % len(self.imgSet[self.action][self.direction])
            self.img = self.imgSet[self.action][self.direction][self.imgNo]
            self.imgSize = self.img.get_size()

    def changeDirection(self, val):
        self.direction = val
        self.img = self.imgSet[self.action][self.direction][self.imgNo]
        self.imgSize = self.img.get_size()

    def changeAction(self, val):
        self.action = val
        self.imgStep = 0
        self.imgNo = 0
        self.img = self.imgSet[self.action][self.direction][self.imgNo]
        self.imgSize = self.img.get_size()

class Character(Unit):
    def __init__(self, imgSet, loc, Class, level, hp, jump, moveDist, attackDist):
        super(Character, self).__init__(imgSet, loc, Class, level, hp, jump, moveDist, attackDist)
        self.locStore = loc #Used to cancel a move (update when move is finalized)
        self.gauges = []

class Enemy(Unit):
    def __init__(self, imgSet, loc, Class, level, hp, jump, moveDist, attackDist):
        super(Enemy, self).__init__(imgSet, loc, Class, level, hp, jump, moveDist, attackDist)

class Object(Unit):
    def __init__(self, imgSet, loc):
        self.imgSet = imgSet
        self.loc = loc

        self.action = "idle"
        self.direction = 0 #0 = down-right, clockwise
        self.imgNo = 0
        self.imgStep = 0
        self.img = self.imgSet[self.action][self.direction][self.imgNo]
        self.imgSize = self.img.get_size()

class Gauge():
    def __init__(self, element, capacity):
        self.element = element
        self.maxCap = capacity
        self.cap = capacity

class Animation():
    def __init__(self, Type, loc, imgSet, imgSpeed = 3):
        self.Type = Type
        self.locBase = loc
        self.imgSet = imgSet
        self.img = self.imgSet[0]
        self.imgSize = self.img.get_size()
        self.loc = [self.locBase[0] - self.imgSize[0]//2, self.locBase[1] - self.imgSize[1]//2]
        self.imgSpeed = imgSpeed
        self.imgStep = 0
        self.imgNo = 0
        self.imgSpeed = imgSpeed

    def tickImg(self):
        self.imgStep += 1
        if self.imgStep == self.imgSpeed:
            self.imgStep = 0
            self.imgNo += 1
            if self.imgNo == len(self.imgSet):
                return True
            else:
                self.img = self.imgSet[self.imgNo]
                self.imgSize = self.img.get_size()
                self.loc = self.locBase
                self.loc = [self.locBase[0] - self.imgSize[0]//2, self.locBase[1] - self.imgSize[1]//2]
